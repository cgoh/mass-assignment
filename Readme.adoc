= Mass Assignment

//tag::abstract[]

Mass Assignment happens when additional
relevant parameters are accepted by the program which 
can update relevant data model fields.
Mass Assignment can result in overwriting an existing field,
or injecting into sensitive fields.

//end::abstract[]

This vulnerability is commonly identified in programming
language frameworks where a data model can be populated
via HTTP requests and sensitive fields are not ignored.

//tag::lab[]

== Lab

Your objective in each lab is to pass the build.

=== Task 0

*Fork* and clone this repository.
Install `docker` and `make` on your system.

. Build the program: `make build`.
. Run it: `make run`.
. Run unit tests: `make test`. 
. Run security tests: `make securitytest`.

Note: The last test will fail. 

=== Task 1

Review the program code try to find out 
why security tests fails. 

Note: Avoid looking at tests or patch file and try to
spot the vulnerable pattern on your own.

=== Task 2

The program is vulnerable to mass assignment. 
Find how to patch this vulnerability.

=== Task 3

Review `test/java/appSecuritySpec.java` and see how security tests
works. Review your patch from Task 2.
Make sure this time the security tests pass.
If you stuck, move to the next task.

=== Task 4

Check out the `patch` branch and review the program code.
Run all tests and make sure everything pass.

=== Task 5

Merge the patch branch to master.
Commit and push your changes.
Does pipeline show that you have passed the build? 

(Note: you do NOT need to send a pull request)

//end::lab[]

//tag::references[]

== References

* OWASP Application Security Verification Standard 4.0, 5.1.2
* CWE 235
* https://github.com/OWASP/CheatSheetSeries/blob/a5ab44faca75dbbb2517583c337d010579d7ce81/cheatsheets/Mass_Assignment_Cheat_Sheet.md[Mass_Assignment_Cheat_Sheet.md]
* https://www.baeldung.com/jackson-ignore-properties-on-serialization[Jackson Ignore Properties on Marshalling]


//end::references[]
