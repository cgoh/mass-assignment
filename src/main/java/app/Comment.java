package app;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Represents a comment submitted by a user
 */
@JsonIgnoreProperties(value = { "id","approved" })
public class Comment {
    
    /**
     * Comment identifier
     */ 
    private long id;

    /**
     * Content of the comment
     */
    private String content;

    /**
     * if comment is approved or not by moderator
     */
    private String approved;

    public Comment(){
        super();
    }

    public Comment(final long id, final String content, final String approved) {
        this.id = id;
        this.content = content;
        this.approved = approved;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public String getApproved() {
        return approved;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public void setApproved(final String approved) {
        this.approved = approved;
    }

    @Override
    public String toString() {
        return this.content;
    }
}
